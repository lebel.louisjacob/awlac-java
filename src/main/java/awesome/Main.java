package awesome;

import awesome.output.Application;

public final class Main
{
    private Main()
    {}

    public static void main(final String[] arguments)
    {
        final var application = Application.of(arguments);
        application.run();
    }
}
