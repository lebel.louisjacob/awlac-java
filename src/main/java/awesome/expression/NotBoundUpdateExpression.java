package awesome.expression;

import awesome.expression.field.Field;
import com.google.common.collect.ImmutableSet;
import org.jetbrains.annotations.NotNull;

public final class NotBoundUpdateExpression extends NotBoundExpression
{
    public static Expression of(final ImmutableSet<Field> fields)
    {
        return new NotBoundUpdateExpression(fields);
    }

    private final ImmutableSet<Field> fields;

    private NotBoundUpdateExpression(final ImmutableSet<Field> fields)
    {
        this.fields = fields;
    }

    @Override
    public Expression copy()
    {
        throw new NotBoundUpdateSyntaxException();
    }

    @Override
    public void setContext(final @NotNull Field parent)
    {
        throw new NotBoundUpdateSyntaxException();
    }

    @Override
    public Expression visitForConcatNotBound(final Expression visitor)
    {
        return UpdateExpression.of(visitor, ObjectExpression.ownerOf(this.fields));
    }

    @Override
    public TerminalExpression reduce()
    {
        throw new NotBoundUpdateSyntaxException();
    }
}
