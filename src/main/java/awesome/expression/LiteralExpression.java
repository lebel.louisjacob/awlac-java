package awesome.expression;

import awesome.expression.field.Field;
import org.jetbrains.annotations.NonNls;
import org.jetbrains.annotations.NotNull;

public final class LiteralExpression extends TerminalExpression
{
    public static TerminalExpression of(final String literal)
    {
        return new LiteralExpression(literal);
    }

    @NonNls
    private final String literal;

    private LiteralExpression(final String literal)
    {
        this.literal = literal;
    }

    @Override
    public TerminalExpression copy()
    {
        return this;
    }

    @Override
    public void setContext(final @NotNull Field parent)
    {}

    @Override
    public TerminalExpression reduce()
    {
        return this;
    }

    @Override
    public TerminalExpression concat(final TerminalExpression other)
    {
        final var otherLiteral = other.toLiteral();
        return new LiteralExpression(this.literal + otherLiteral);
    }

    @Override
    public String toLiteral()
    {
        return this.literal;
    }

    @Override
    public ObjectExpression toObject()
    {
        throw new InvalidObjectCastException(this);
    }

    @Override
    public String toString()
    {
        return String.format("'%s'", this.literal);
    }
}
