package awesome.expression;

import awesome.expression.field.Field;
import org.jetbrains.annotations.NotNull;

public final class AccessExpression extends Expression
{
    public static Expression of(final Expression expression, final String name)
    {
        return new AccessExpression(expression, name);
    }

    private final Expression expression;
    private final String name;

    private AccessExpression(final Expression expression, final String name)
    {
        this.expression = expression;
        this.name = name;
    }

    @Override
    public Expression copy()
    {
        return new AccessExpression(this.expression.copy(), this.name);
    }

    @Override
    public void setContext(final @NotNull Field parent)
    {
        this.expression.setContext(parent);
    }

    @Override
    public TerminalExpression reduce()
    {
        return this.expression.reduce()
            .toObject()
            .get(this.name)
            .reduce();
    }

    @Override
    public String toString()
    {
        return String.format("%s.%s", this.expression, this.name);
    }
}
