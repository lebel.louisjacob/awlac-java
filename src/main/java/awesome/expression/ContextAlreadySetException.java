package awesome.expression;

public class ContextAlreadySetException extends RuntimeException
{
    public ContextAlreadySetException()
    {
        super("should set parent of field only once");
    }
}
