package awesome.expression.field;

public class DuplicatedReferenceException extends RuntimeException
{
    DuplicatedReferenceException(final String name)
    {
        super(String.format("found \"%s\" multiple times in the context", name));
    }
}
