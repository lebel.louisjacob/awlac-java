package awesome.expression.field;

public class ReferenceNotFoundException extends RuntimeException
{
    public ReferenceNotFoundException(final String name)
    {
        super(String.format("could not find '%s' in context", name));
    }
}
