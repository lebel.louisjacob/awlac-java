package awesome.expression.field;

import awesome.expression.Expression;
import awesome.expression.ObjectExpression;
import awesome.expression.TerminalExpression;

import java.util.Objects;

public abstract class Field
{
    public static final String literalFieldName = "''";

    public static Field of(final String name, final Expression expression)
    {
        final var newField = new ObjectField(name, expression);
        expression.setContext(newField);
        return newField;
    }

    public static Field literalOf(final Expression expression)
    {
        return of(literalFieldName, expression);
    }

    public abstract void setContainer(ObjectExpression container);
    public abstract void setContext(Field context);

    public abstract Field copy();

    public abstract String getName();
    public abstract TerminalExpression reduce();

    public abstract boolean canFind(String name);
    public abstract Field find(String name);

    @Override
    public final boolean equals(final Object other)
    {
        final var otherField = (Field) other;
        final var name = otherField.getName();
        return this.getName().equals(name);
    }

    @Override
    public final int hashCode()
    {
        final var name = this.getName();
        return Objects.hashCode(name);
    }
}
