package awesome.expression.field;

class ContainerAlreadySetException extends RuntimeException
{
    ContainerAlreadySetException()
    {
        super("should set container of field only once");
    }
}
