package awesome.expression;

import awesome.expression.field.Field;
import org.jetbrains.annotations.NotNull;

public abstract class Expression
{
    public abstract void setContext(@NotNull Field parent);
    public abstract Expression copy();
    public abstract TerminalExpression reduce();
    public final Expression concatNotBound(final Expression other)
    {
        try
        {
            return ((NotBoundExpression)other).visitForConcatNotBound(this);
        }
        catch(final ClassCastException e)
        {
            return ConcatExpression.of(this, other);
        }
    }
}
