package awesome.expression;

public abstract class NotBoundExpression extends Expression
{
    public abstract Expression visitForConcatNotBound(Expression visitor);
}
