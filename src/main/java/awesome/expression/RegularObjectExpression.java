package awesome.expression;

import awesome.expression.field.Field;
import com.google.common.collect.ImmutableSet;
import org.jetbrains.annotations.NotNull;

import java.util.stream.Collectors;

import static awesome.expression.field.Field.literalFieldName;
import static com.google.common.base.Predicates.compose;
import static com.google.common.base.Predicates.equalTo;
import static com.google.common.base.Predicates.not;

public final class RegularObjectExpression extends ObjectExpression
{
    private final ImmutableSet<Field> fields;

    RegularObjectExpression(final ImmutableSet<Field> fields)
    {
        this.fields = fields;
    }

    @Override
    public ObjectExpression copy()
    {
        final var newFields = this.copyFields();
        return ObjectExpression.ownerOf(newFields);
    }

    private ImmutableSet<Field> copyFields()
    {
        final var newFields = ImmutableSet.<Field>builder();
        for(final var field: this.fields)
        {
            final var newField = field.copy();
            newFields.add(newField);
        }
        return newFields.build();
    }

    @Override
    public TerminalExpression update(final TerminalExpression other)
    {
        final var newFields = other.toObject().updateFields(this.fields);
        return ObjectExpression.ownerOf(newFields);
    }

    @Override
    ImmutableSet<Field> updateFields(final ImmutableSet<Field> previousFields)
    {
        for(final var field: this.fields)
        {
            if(!previousFields.contains(field))
            {
                throw new IllegalUpdateException(this, field);
            }
        }
        return ImmutableSet.<Field>builder()
            .addAll(previousFields.stream()
                .filter(not(this.fields::contains))
                .collect(Collectors.toSet()))
            .addAll(this.fields)
            .build();
    }

    @Override
    public TerminalExpression concat(final TerminalExpression other)
    {
        return other.toObject().concatFieldsTo(this.fields);
    }

    @Override
    ObjectExpression concatFieldsTo(final ImmutableSet<Field> previousFields)
    {
        final var concatObject = ObjectExpression.of(
            ImmutableSet.<Field>builder()
                .addAll(previousFields)
                .addAll(this.fields)
                .build());
        for(final var child: this.fields)
        {
            child.setContainer(concatObject);
        }
        return concatObject;
    }

    @Override
    public void setContext(final @NotNull Field parent)
    {
        for(final var field: this.fields.asList())
        {
            field.setContext(parent);
        }
    }

    @Override
    public TerminalExpression reduce()
    {
        return this;
    }

    @Override
    public String toLiteral()
    {
        if(!this.contains(literalFieldName))
        {
            throw new InvalidLiteralCastException(this);
        }
        return this.get(literalFieldName)
            .reduce()
            .toLiteral();
    }

    @Override
    public ObjectExpression toObject()
    {
        return this;
    }

    @Override
    public Field get(final String name)
    {
        return this.fields.stream()
            .filter(compose(equalTo(name), Field::getName))
            .findAny()
            .orElseThrow(() -> new FieldNotFoundException(this, name));
    }

    @Override
    public boolean contains(final String name)
    {
        return this.fields.stream().anyMatch(compose(equalTo(name), Field::getName));
    }

    @Override
    public String toString()
    {
        final var rawFieldsString = this.fields.toString();
        final var fieldsLength = rawFieldsString.length();
        final var fieldsString = rawFieldsString.substring(1, fieldsLength - 1);
        return String.format("{%s}", fieldsString);
    }
}
