package awesome.expression;

import awesome.expression.field.Field;
import com.google.common.collect.ImmutableSet;

public abstract class ObjectExpression extends TerminalExpression
{
    public static ObjectExpression ownerOf(final ImmutableSet<Field> fields)
    {
        final var object = new RegularObjectExpression(fields);
        for(final var field: fields)
        {
            field.setContainer(object);
        }
        return object;
    }

    public static ObjectExpression of(final ImmutableSet<Field> fields)
    {
        return new RegularObjectExpression(fields);
    }

    @Override
    public abstract ObjectExpression copy();

    public abstract TerminalExpression update(TerminalExpression other);
    abstract ImmutableSet<Field> updateFields(ImmutableSet<Field> previousFields);
    abstract ObjectExpression concatFieldsTo(ImmutableSet<Field> previousFields);

    public abstract Field get(String name);
    public abstract boolean contains(String name);
}
