package awesome.expression;

class IllegalConcatException extends RuntimeException
{
    IllegalConcatException(final ObjectExpression object, final String name)
    {
        super(String.format("cannot insert duplicate '%s' in '%s'", name, object));
    }
}
