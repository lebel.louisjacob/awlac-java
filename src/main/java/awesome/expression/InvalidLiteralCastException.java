package awesome.expression;

class InvalidLiteralCastException extends RuntimeException
{
    InvalidLiteralCastException(final Expression expression)
    {
        super(String.format("cannot cast %s to a literal", expression));
    }
}
