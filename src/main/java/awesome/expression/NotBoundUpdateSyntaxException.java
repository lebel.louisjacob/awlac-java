package awesome.expression;

class NotBoundUpdateSyntaxException
    extends RuntimeException
{
    NotBoundUpdateSyntaxException()
    {
        super("cannot update nothing");
    }
}
