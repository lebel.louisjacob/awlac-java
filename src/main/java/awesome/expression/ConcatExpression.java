package awesome.expression;

import awesome.expression.field.Field;
import org.jetbrains.annotations.NotNull;

final class ConcatExpression extends Expression
{
    public static Expression of(
        final Expression left,
        final Expression right)
    {
        return new ConcatExpression(left, right);
    }

    private final Expression left;
    private final Expression right;

    private ConcatExpression(
        final Expression left,
        final Expression right)
    {
        this.left = left;
        this.right = right;
    }

    @Override
    public Expression copy()
    {
        return new ConcatExpression(this.left.copy(), this.right.copy());
    }

    @Override
    public void setContext(final @NotNull Field parent)
    {
        this.left.setContext(parent);
        this.right.setContext(parent);
    }

    @Override
    public TerminalExpression reduce()
    {
        final var reducedLeft = this.left.reduce();
        final var reducedRight = this.right.reduce();
        return reducedLeft.concat(reducedRight);
    }

    @Override
    public String toString()
    {
        return String.format("%s::%s", this.left, this.right);
    }
}
