package awesome.expression;

class InvalidObjectCastException extends RuntimeException
{
    InvalidObjectCastException(final Expression expression)
    {
        super(String.format("cannot cast %s to an object", expression));
    }
}
