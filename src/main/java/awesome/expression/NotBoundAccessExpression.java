package awesome.expression;

import awesome.expression.field.Field;
import org.jetbrains.annotations.NotNull;

public final class NotBoundAccessExpression extends NotBoundExpression
{
    public static NotBoundExpression of(final String name)
    {
        return new NotBoundAccessExpression(name);
    }

    private final String name;

    private NotBoundAccessExpression(final String name)
    {
        this.name = name;
    }

    @Override
    public Expression copy()
    {
        throw new NotBoundUpdateSyntaxException();
    }

    @Override
    public void setContext(final @NotNull Field parent)
    {
        throw new NotBoundUpdateSyntaxException();
    }

    @Override
    public Expression visitForConcatNotBound(final Expression visitor)
    {
        return AccessExpression.of(visitor, this.name);
    }

    @Override
    public TerminalExpression reduce()
    {
        throw new NotBoundAccessSyntaxException(this);
    }
}
