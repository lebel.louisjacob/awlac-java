package awesome.expression;

import awesome.expression.field.Field;

class IllegalUpdateException extends RuntimeException
{
    IllegalUpdateException(final ObjectExpression object, final Field field)
    {
        super(String.format("could not find \"%s\" in \"%s\"", field, object));
    }
}
