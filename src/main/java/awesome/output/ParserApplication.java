package awesome.output;

import awesome.expression.Expression;
import awesome.expression.ObjectExpression;
import awesome.expression.field.Field;
import awesome.parser.Parser;
import com.google.common.collect.ImmutableSet;

import java.nio.file.Path;

final class ParserApplication implements Application
{
    private final Path contextPath;
    private final Path outputFile;
    private final String programSource;

    ParserApplication(final Path contextPath, final Path outputFile, final String programSource)
    {
        this.contextPath = contextPath;
        this.outputFile = outputFile;
        this.programSource = programSource;
    }

    @Override
    public void run()
    {
        final var expression = this.parse();
        Application.compilerOf(expression, this.outputFile).run();
    }

    private Expression parse()
    {
        final var program = this.parseProgram();
        final var context = this.parseContext();
        return ObjectExpression.ownerOf(
            ImmutableSet.of(
                program,
                context));
    }

    private Field parseProgram()
    {
        final var programParser = Parser.expressionOf(this.programSource);
        final var parsedProgram = programParser.parse();
        return Field.literalOf(parsedProgram);
    }

    private Field parseContext()
    {
        final var contextParser = Parser.pathOf(this.contextPath);
        return contextParser.parse();
    }
}
