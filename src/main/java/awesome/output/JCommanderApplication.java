package awesome.output;

import com.beust.jcommander.JCommander;
import com.beust.jcommander.Parameter;
import com.beust.jcommander.converters.PathConverter;

import java.nio.file.Path;
import java.util.ResourceBundle;

final class JCommanderApplication implements Application
{
    private static final Path workingDirectory = Path.of(System.getProperty("user.dir"));
    private static final String defaultOutputFileName = "awesome.out";

    @Parameter(names = {"-h", "--help"}, descriptionKey = "help.description", help = true)
    private boolean isHelp;

    @Parameter(names = {"-c", "--context"}, descriptionKey = "context.description", converter = PathConverter.class)
    private Path contextPath;

    @Parameter(names = {"-o", "--output"}, descriptionKey = "output.description", converter = PathConverter.class)
    private Path outputFile;

    @Parameter(descriptionKey = "input.usage")
    private String programSource;

    private final JCommander commander;

    JCommanderApplication(final String[] arguments)
    {
        this.isHelp = false;
        this.contextPath = workingDirectory;
        this.outputFile = workingDirectory.resolve(JCommanderApplication.defaultOutputFileName);
        this.programSource = "";

        final var terminalBundle = ResourceBundle.getBundle("terminal");
        this.commander = JCommander.newBuilder()
            .programName(terminalBundle.getString("name"))
            .addObject(this)
            .args(arguments)
            .resourceBundle(terminalBundle)
            .build();
    }

    @Override
    public void run()
    {
        if(this.isHelp)
        {
            this.printJCommanderUsage();
            return;
        }
        Application.parserOf(this.contextPath, this.outputFile, this.programSource).run();
    }

    private void printJCommanderUsage()
    {
        this.commander.usage();
    }
}
