package awesome.output;

import awesome.expression.Expression;

import java.nio.file.Path;

public interface Application extends Runnable
{
    static Application of(final String[] arguments)
    {
        return new JCommanderApplication(arguments);
    }

    static Application fileOverwriteOf(final Path fileToOverwrite, final String newContent)
    {
        return new FileOverwriteApplication(fileToOverwrite, newContent);
    }

    static Application compilerOf(final Expression expression, final Path outputFile)
    {
        return new CompilerApplication(expression, outputFile);
    }

    static Application parserOf(final Path contextPath, final Path outputFile, final String programSource)
    {
        return new ParserApplication(contextPath, outputFile, programSource);
    }
}
