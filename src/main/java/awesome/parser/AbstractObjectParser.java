package awesome.parser;

import awesome.expression.Expression;
import awesome.expression.field.Field;
import com.google.common.collect.ImmutableSet;
import org.intellij.lang.annotations.Language;
import string.CutException;
import string.StringCutter;

abstract class AbstractObjectParser implements Parser<String, Expression>
{
    @Language("RegExp")
    static final String keySeparatorRegex = "\\s*:\\s*";
    @Language("RegExp")
    private static final String valueSeparatorRegex = "\\s*";

    private static final StringCutter keySeparatorCutter = StringCutter.regex(keySeparatorRegex);
    private static final StringCutter valueSeparatorCutter = StringCutter.regex(valueSeparatorRegex);

    private final String initialInput;

    AbstractObjectParser(final String initialInput)
    {
        this.initialInput = initialInput;
    }

    @Override
    public final Expression parse()
    {
        final var startRemainder = this.getStartRemainder(this.initialInput);
        final var fields = this.parseAllFields(ImmutableSet.builder(), startRemainder);
        return this.parseFields(fields);
    }

    abstract Expression parseFields(final ImmutableSet<Field> fields);

    private ImmutableSet<Field> parseAllFields(
        final ImmutableSet.Builder<Field> fieldsBuilder,
        final String remainder)
    {
        try
        {
            fieldsBuilder.add(parseNextField(remainder));
            return this.parseAllFields(
                fieldsBuilder,
                getFieldRemainder(remainder));
        }
        catch(final CutException ignored)
        {}
        return fieldsBuilder.build();
    }

    private static Field parseNextField(final String remainder)
    {
        final var name = parseName(remainder);
        final var expression = parseExpression(remainder);
        return Field.of(name, expression);
    }

    private static String parseName(final String remainder)
    {
        return ReferenceParser.referenceCutter.cutThenGetHead(remainder);
    }

    private static Expression parseExpression(final String remainder)
    {
        final var newRemainder = getKeyRemainder(remainder);
        return getExpressionParser(newRemainder).parse();
    }

    @Override
    public final String getRemainder()
    {
        final var startRemainder = this.getStartRemainder(this.initialInput);
        final var fieldsRemainder = this.getAllFieldsRemainder(startRemainder);
        return this.getEndRemainder(fieldsRemainder);
    }

    abstract String getStartRemainder(final String remainder);

    abstract String getEndRemainder(final String remainder);

    private String getAllFieldsRemainder(final String remainder)
    {
        try
        {
            final var newRemainder = getFieldRemainder(remainder);
            return this.getAllFieldsRemainder(newRemainder);
        }
        catch(final CutException e)
        {
            return remainder;
        }
    }

    private static String getFieldRemainder(final String remainder)
    {
        final var keyRemainder = getKeyRemainder(remainder);
        return getExpressionRemainder(keyRemainder);
    }

    private static String getKeyRemainder(final String remainder)
    {
        final var keyRemainder = ReferenceParser.referenceCutter.cutThenGetRemainder(remainder);
        return AbstractObjectParser.keySeparatorCutter.cutThenGetRemainder(keyRemainder);
    }

    private static String getExpressionRemainder(final String remainder)
    {
        final var valueParserRemainder = getExpressionParser(remainder).getRemainder();
        return AbstractObjectParser.valueSeparatorCutter.cutThenGetRemainder(valueParserRemainder);
    }

    private static Parser<String, Expression> getExpressionParser(final String remainder)
    {
        return Parser.expressionOf(remainder);
    }
}
