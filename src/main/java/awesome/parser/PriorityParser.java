package awesome.parser;

import awesome.expression.Expression;
import org.intellij.lang.annotations.Language;
import string.StringCutter;

final class PriorityParser implements Parser<String, Expression>
{
    @Language("RegExp")
    static final String startRegex = "[(]\\s*";
    @Language("RegExp")
    static final String endRegex = "\\s*[)]";

    private static final StringCutter startCutter = StringCutter.regex(PriorityParser.startRegex);
    private static final StringCutter endCutter = StringCutter.regex(PriorityParser.endRegex);

    private final String initialInput;

    PriorityParser(final String initialInput)
    {
        this.initialInput = initialInput;
    }

    @Override
    public Expression parse()
    {
        final var startRemainder = PriorityParser.startCutter.cutThenGetRemainder(this.initialInput);
        final var parser = Parser.expressionOf(startRemainder);
        return parser.parse();
    }

    @Override
    public String getRemainder()
    {
        final var startRemainder = PriorityParser.startCutter.cutThenGetRemainder(this.initialInput);
        final var parser = Parser.expressionOf(startRemainder);
        final var parserRemainder = parser.getRemainder();
        return PriorityParser.endCutter.cutThenGetRemainder(parserRemainder);
    }
}
