package awesome.parser;

import awesome.expression.Expression;
import awesome.expression.LiteralExpression;
import org.intellij.lang.annotations.Language;
import org.jetbrains.annotations.NonNls;
import string.StringCutter;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

final class LiteralParser implements Parser<String, Expression>
{
    @NonNls
    static final String quote = "'";
    @NonNls
    private static final String escape = "\\";

    private static final String escapedQuote = escape + quote;
    private static final String escapedEscape = escape + escape;

    @NonNls
    @Language("RegExp")
    private static final String contentRegex
        = String.format("([^%s]|%s.)*",
            Pattern.quote(LiteralParser.escape + LiteralParser.quote),
            Pattern.quote(LiteralParser.escape));

    private static final StringCutter quoteCutter = StringCutter.literal(quote);
    private static final StringCutter contentCutter = StringCutter.regex(contentRegex);

    private final String initialInput;

    LiteralParser(final String initialInput)
    {
        this.initialInput = initialInput;
    }

    @Override
    public Expression parse()
    {
        final var firstQuoteRemainder = getQuoteRemainder(this.initialInput);
        final var literal = parseContent(firstQuoteRemainder);
        return LiteralExpression.of(literal);
    }

    private static String parseContent(final String remainder)
    {
        final var rawLiteral = LiteralParser.contentCutter.cutThenGetHead(remainder);
        return parseContentEscapes(rawLiteral);
    }

    private static String parseContentEscapes(final String rawLiteral)
    {
        return rawLiteral
            .replaceAll(
                Pattern.quote(LiteralParser.escapedQuote),
                Matcher.quoteReplacement(LiteralParser.quote))
            .replaceAll(
                Pattern.quote(LiteralParser.escapedEscape),
                Matcher.quoteReplacement(LiteralParser.escape));
    }

    @Override
    public String getRemainder()
    {
        final var firstQuoteRemainder = getQuoteRemainder(this.initialInput);
        final var contentRemainder = getContentRemainder(firstQuoteRemainder);
        return getQuoteRemainder(contentRemainder);
    }

    private static String getQuoteRemainder(final String remainder)
    {
        return LiteralParser.quoteCutter.cutThenGetRemainder(remainder);
    }

    private static String getContentRemainder(final String remainder)
    {
        return LiteralParser.contentCutter.cutThenGetRemainder(remainder);
    }
}
