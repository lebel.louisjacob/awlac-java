package awesome.parser;

import awesome.expression.NotBoundAccessExpression;
import awesome.expression.Expression;
import org.intellij.lang.annotations.Language;
import string.StringCutter;

final class AccessParser implements Parser<String, Expression>
{
    @Language("RegExp")
    static final String separatorRegex = "\\s*[.]\\s*";
    private static final StringCutter accessSeparatorCutter
        = StringCutter.regex(AccessParser.separatorRegex);

    private final String initialInput;

    AccessParser(final String initialInput)
    {
        this.initialInput = initialInput;
    }

    @Override
    public Expression parse()
    {
        final var accessRemainder = getAccessRemainder(this.initialInput);
        final var key = parseKey(accessRemainder);
        return NotBoundAccessExpression.of(key);
    }

    private static String parseKey(final String remainder)
    {
        return ReferenceParser.referenceCutter.cutThenGetHead(remainder);
    }

    @Override
    public String getRemainder()
    {
        final var accessRemainder = getAccessRemainder(this.initialInput);
        return getKeyRemainder(accessRemainder);
    }

    private static String getAccessRemainder(final String remainder)
    {
        return accessSeparatorCutter.cutThenGetRemainder(remainder);
    }

    private static String getKeyRemainder(final String remainder)
    {
        return ReferenceParser.referenceCutter.cutThenGetRemainder(remainder);
    }
}
