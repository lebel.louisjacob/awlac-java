package awesome.parser;

import string.CutException;

import java.util.regex.Pattern;

class MissingExpressionException extends CutException
{
    MissingExpressionException(final String fullInput, final String remainder)
    {
        super(
            fullInput.replaceAll(
                String.format("(%s)$", Pattern.quote(remainder)),
                ">>>HERE<<<$1"));
    }
}
