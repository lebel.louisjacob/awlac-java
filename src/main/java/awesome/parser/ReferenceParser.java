package awesome.parser;

import awesome.expression.Expression;
import awesome.expression.ReferenceExpression;
import org.intellij.lang.annotations.Language;
import string.StringCutter;

final class ReferenceParser implements Parser<String, Expression>
{
    @Language("RegExp")
    static final String referenceRegex = "[^\\[\\](){}<>.:'\\u0b00\t\n \r\f]+";
    static final StringCutter referenceCutter = StringCutter.regex(referenceRegex);

    private final String initialInput;

    ReferenceParser(final String initialInput)
    {
        this.initialInput = initialInput;
    }

    @Override
    public Expression parse()
    {
        final var name = ReferenceParser.referenceCutter.cutThenGetHead(this.initialInput);
        return ReferenceExpression.of(name);
    }

    @Override
    public String getRemainder()
    {
        return ReferenceParser.referenceCutter.cutThenGetRemainder(this.initialInput);
    }
}
