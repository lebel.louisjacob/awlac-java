package awesome.parser;

import awesome.expression.Expression;
import com.google.common.collect.ImmutableList;
import org.intellij.lang.annotations.Language;
import string.CutException;
import string.StringCutter;

import java.util.Optional;

final class ExpressionParser implements Parser<String, Expression>
{
    @Language("RegExp")
    private static final String concatenationSeparatorRegex = "::";

    @Language("RegExp")
    private static final String concatenationRegex =
        "\\s*"
        + "(?:"
            + concatenationSeparatorRegex
        + '|'
            + "(?="
                + AccessParser.separatorRegex
            + '|'
                + PriorityParser.startRegex
            + '|'
                + UpdateParser.startRegex
            + '|'
                + ObjectParser.startRegex
            + '|'
                + LiteralParser.quote
            + '|'
                + ReferenceParser.referenceRegex
            + ')'
        + ')'
        + "(?!"
            + ReferenceParser.referenceRegex
            + AbstractObjectParser.keySeparatorRegex
        + ')'
        + "\\s*";

    private static final StringCutter concatenationCutter = StringCutter.regex(concatenationRegex);

    private final String initialInput;

    ExpressionParser(final String initialInput)
    {
        this.initialInput = initialInput;
    }

    @Override
    public Expression parse()
    {
        final var tokensBuilder = ImmutableList.<Expression>builder();
        final var newRemainder = this.parseNext(tokensBuilder, this.initialInput);
        final var expressions = this.parseAll(tokensBuilder, newRemainder);
        return expressions.stream()
            .reduce(Expression::concatNotBound)
            .orElseThrow(() -> new MissingExpressionException(this.initialInput, this.initialInput));
    }

    private ImmutableList<Expression> parseAll(
        final ImmutableList.Builder<Expression> tokensBuilder,
        final String remainder)
    {
        return getSeparatorRemainder(remainder)
            .map(newRemainder -> this.parseNext(tokensBuilder, newRemainder))
            .map(newRemainder -> this.parseAll(tokensBuilder, newRemainder))
            .orElseGet(tokensBuilder::build);
    }

    private String parseNext(
        final ImmutableList.Builder<Expression> tokensBuilder,
        final String remainder)
    {
        final var parser = this.findNextParser(remainder);
        tokensBuilder.add(parser.parse());
        return parser.getRemainder();
    }

    private static Optional<String> getSeparatorRemainder(final String remainder)
    {
        try
        {
            return Optional.of(tryGetSeparatorRemainder(remainder));
        }
        catch(final CutException e)
        {
            return Optional.empty();
        }
    }

    private static String tryGetSeparatorRemainder(final String remainder)
    {
        return ExpressionParser.concatenationCutter.cutThenGetRemainder(remainder);
    }

    private Parser<String, ? extends Expression> findNextParser(final String input)
    {
        return getNestedParsers(input).stream()
            .filter(Parser::canParse)
            .findFirst()
            .orElseThrow(() -> new MissingExpressionException(this.initialInput, input));
    }

    private static ImmutableList<Parser<String, ? extends Expression>> getNestedParsers(final String input)
    {
        return ImmutableList.of(
            Parser.referenceOf(input),
            Parser.literalOf(input),
            Parser.objectOf(input),
            Parser.updateOf(input),
            Parser.accessOf(input),
            Parser.priorityOf(input));
    }

    @Override
    public String getRemainder()
    {
        final var remainder = this.getNextRemainder(this.initialInput);
        return this.getFullRemainder(remainder);
    }

    private String getFullRemainder(final String remainder)
    {
        return getSeparatorRemainder(remainder)
            .map(this::getNextRemainder)
            .map(this::getFullRemainder)
            .orElse(remainder);
    }

    private String getNextRemainder(final String remainder)
    {
        return this.findNextParser(remainder).getRemainder();
    }
}
