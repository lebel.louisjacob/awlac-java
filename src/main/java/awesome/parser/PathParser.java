package awesome.parser;

import awesome.expression.Expression;
import awesome.expression.ObjectExpression;
import awesome.expression.field.Field;
import com.google.common.collect.ImmutableSet;

import java.io.IOException;
import java.io.UncheckedIOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.function.Supplier;
import java.util.regex.Pattern;
import java.util.stream.Stream;

final class PathParser implements Parser<Void, Field>
{
    private static final Pattern FILE_EXTENSION_PATTERN = Pattern.compile("[.](?:al|awla|awesome)$");
    private final Path path;
    private final String pathName;

    PathParser(final Path path)
    {
        this.path = path;
        this.pathName = path.getFileName().toString();
    }

    @Override
    public Field parse()
    {
        final Expression expression;
        final String name;
        if(Files.isDirectory(this.path))
        {
            expression = this.parseFolder();
            name = this.pathName;
        }
        else if(Files.isRegularFile(this.path))
        {
            expression = this.parseFile();
            name = this.parseFileName();
        }
        else
        {
            throw new UncheckedIOException(new IOException("cannot parse unknown path type"));
        }
        return Field.of(name, expression);
    }

    private Expression parseFolder()
    {
        try(final var content = Files.list(this.path))
        {
            final var objectBuilder = ImmutableSet.<Field>builder();
            content.filter(PathParser::isValidAndVisiblePath)
                    .map(Parser::pathOf)
                    .flatMap(parser -> flatMapIgnoreExtensionException(parser::parse))
                    .forEach(objectBuilder::add);
            return ObjectExpression.ownerOf(objectBuilder.build());
        }
        catch(final IOException e)
        {
            throw new UncheckedIOException(e);
        }
    }

    private static boolean isValidAndVisiblePath(final Path path)
    {
        try
        {
            return !Files.isHidden(path) && isValidPath(path);
        }
        catch(final IOException e)
        {
            return false;
        }
    }

    private static boolean isValidPath(final Path path)
    {
        return isValidFile(path) || isValidFolder(path);
    }

    private static boolean isValidFile(final Path path)
    {
        return Files.isRegularFile(path)
            && Stream.of(".al", ".awla", ".awesome")
                .anyMatch(path.getFileName().toString()::endsWith);
    }

    private static boolean isValidFolder(final Path path)
    {
        return Files.isDirectory(path);
    }

    private static <Type> Stream<Type> flatMapIgnoreExtensionException(
        final Supplier<? extends Type> supplier)
    {
        try
        {
            return Stream.of(supplier.get());
        }
        catch(final BadFileNameExtensionException ignored)
        {
            return Stream.empty();
        }
    }

    private Expression parseFile()
    {
        try
        {
            return Parser.expressionOf(Files.readString(this.path, StandardCharsets.UTF_8)).parse();
        }
        catch(final IOException e)
        {
            throw new UncheckedIOException(e);
        }
    }

    private String parseFileName()
    {
        return this.removeFileNameExtension();
    }

    private String removeFileNameExtension()
    {
        return FILE_EXTENSION_PATTERN
            .matcher(this.pathName)
            .replaceFirst("");
    }

    @Override
    public Void getRemainder()
    {
        return null;
    }
}
