package awesome.parser;

import awesome.expression.Expression;
import awesome.expression.NotBoundUpdateExpression;
import awesome.expression.field.Field;
import com.google.common.collect.ImmutableSet;
import org.intellij.lang.annotations.Language;
import string.StringCutter;

final class UpdateParser extends AbstractObjectParser
{
    @Language("RegExp")
    static final String startRegex = "\\[\\s*";
    @Language("RegExp")
    private static final String endRegex = "\\s*]";

    private static final StringCutter updateStartCutter = StringCutter.regex(UpdateParser.startRegex);
    private static final StringCutter updateEndCutter = StringCutter.regex(UpdateParser.endRegex);

    UpdateParser(final String initialInput)
    {
        super(initialInput);
    }

    @Override
    Expression parseFields(final ImmutableSet<Field> fields)
    {
        return NotBoundUpdateExpression.of(fields);
    }

    @Override
    String getStartRemainder(final String remainder)
    {
        return UpdateParser.updateStartCutter.cutThenGetRemainder(remainder);
    }

    @Override
    String getEndRemainder(final String remainder)
    {
        return UpdateParser.updateEndCutter.cutThenGetRemainder(remainder);
    }
}
