package string;

public interface CutResult
{
    static CutResult of(final String remainder, final String head)
    {
        return new RegularCutResult(remainder, head);
    }

    String getRemainder();

    String getHead();
}
