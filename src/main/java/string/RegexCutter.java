package string;

import java.util.regex.Pattern;

final class RegexCutter implements StringCutter
{
    private final Pattern pattern;

    RegexCutter(final String regex)
    {
        this.pattern = Pattern.compile(String.format("^(%s)", regex), Pattern.DOTALL);
    }

    @Override
    public CutResult cut(final String input)
    {
        final var head = this.cutHead(input);
        final var remainder = input.replaceFirst(Pattern.quote(head), "");

        return CutResult.of(remainder, head);
    }

    private String cutHead(final String input)
    {
        final var matcher = this.pattern.matcher(input);
        if(!matcher.find())
        {
            throw new CutException();
        }
        return matcher.group();
    }
}
