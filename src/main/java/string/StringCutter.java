package string;

import javax.annotation.RegEx;
import java.util.regex.Pattern;

public interface StringCutter
{
    static StringCutter regex(@RegEx final String regex)
    {
        return new RegexCutter(regex);
    }

    static StringCutter literal(final String literal)
    {
        return StringCutter.regex(Pattern.quote(literal));
    }

    CutResult cut(final String input);

    default String cutThenGetRemainder(final String input)
    {
        return this.cut(input)
            .getRemainder();
    }

    default String cutThenGetHead(final String input)
    {
        return this.cut(input)
            .getHead();
    }
}
