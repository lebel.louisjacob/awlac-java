package awesome.parser;

import org.junit.jupiter.api.Test;
import string.CutException;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

final class LiteralExpressionParserTest
{
    @Test
    void parse_SHOULD_returnLiteralContent_WHEN_inputIsAValidLiteralExpression()
    {
        final var input = "'input'";
        final var aplin = Parser.literalOf(input);

        final var actualToken = aplin.parse();

        final var actual = actualToken.reduce().toLiteral();
        final var expected = "input";
        assertEquals(expected, actual);
    }

    @Test
    void parse_SHOULD_replaceEscapedQuotesWithSimpleQuotes_WHEN_inputContainsEscapedQuotes()
    {
        final var input = "'\\'input\\''";
        final var aplin = Parser.literalOf(input);

        final var actualToken = aplin.parse();

        final var expected = "'input'";
        final var actual = actualToken.reduce().toLiteral();
        assertEquals(expected, actual);
    }

    @Test
    void parse_SHOULD_replaceEscapedEscapesWithSimpleEscapes_WHEN_inputContainsEscapedEscapes()
    {
        final var input = "'\\\\input\\\\'";
        final var aplin = Parser.literalOf(input);

        final var actualToken = aplin.parse();

        final var actual = actualToken.reduce().toLiteral();
        final var expected = "\\input\\";
        assertEquals(expected, actual);
    }

    @Test
    void parse_SHOULD_throwRegexMismatchException_WHEN_inputDoesNotStartWithAValidLiteralExpression()
    {
        final var input = "input'";
        final var aplin = Parser.literalOf(input);

        assertThrows(CutException.class, aplin::parse);
    }

    @Test
    void getRemainder_SHOULD_removeLiteralExpression_WHEN_inputStartsWithAValidLiteralExpression()
    {
        final var input = "'input'getRemainder";
        final var aplin = Parser.literalOf(input);

        final var actual = aplin.getRemainder();

        final var expected = "getRemainder";
        assertEquals(expected, actual);
    }

    @Test
    void getRemainder_SHOULD_throwRegexMismatchException_WHEN_inputDoesNotStartWithAValidLiteralExpression()
    {
        final var input = "input'";
        final var aplin = Parser.literalOf(input);

        assertThrows(CutException.class, aplin::getRemainder);
    }

    @Test
    void canParse_SHOULD_returnTrue_WHEN_inputStartsWithAValidLiteralExpression()
    {
        final var input = "'input'getRemainder";
        final var aplin = Parser.literalOf(input);

        final var actual = aplin.canParse();

        final var expected = true;
        assertEquals(expected, actual);
    }

    @Test
    void canParse_SHOULD_returnFalse_WHEN_inputDoesNotStartWithAValidLiteralExpression()
    {
        final var input = "input'";
        final var aplin = Parser.literalOf(input);

        final var actual = aplin.canParse();

        final var expected = false;
        assertEquals(expected, actual);
    }
}
