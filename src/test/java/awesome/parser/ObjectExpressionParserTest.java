package awesome.parser;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

final class ObjectExpressionParserTest
{
    @Test
    void parse_SHOULD_returnObjectOfOneLiteralField_WHEN_inputIsObjectOfOneLiteralField()
    {
        final var input = "{key: 'value'}";
        final var parser = Parser.objectOf(input);

        final var actualToken = parser.parse();

        final var actual = actualToken.reduce()
                .toObject().get("key").reduce()
                .toLiteral();
        final var expected = "value";
        assertEquals(expected, actual);
    }

    @Test
    void parse_SHOULD_returnObjectOfTwoLiteralFields_WHEN_inputIsObjectOfTwoLiteralFields()
    {
        final var input = "{k0: '0' k1: '1'}";
        final var parser = Parser.objectOf(input);

        final var actualToken = parser.parse();

        final var actual = actualToken.reduce()
                .toObject().get("k1").reduce()
                .toLiteral();
        final var expected = "1";
        assertEquals(expected, actual);
    }

    @Test
    void parse_SHOULD_returnObjectOfThreeLiteralFields_WHEN_inputIsObjectOfThreeLiteralFields()
    {
        final var input = "{k0: '0' k1: '1' k2: '2'}";
        final var parser = Parser.objectOf(input);

        final var actualToken = parser.parse();

        final var actual = actualToken.reduce()
                .toObject().get("k1").reduce()
                .toLiteral();
        final var expected = "1";
        assertEquals(expected, actual);
    }

    @Test
    void parse_SHOULD_returnObjectOfOneObjectField_WHEN_inputIsObjectOfOneObjectField()
    {
        final var input = "{k0: {k0k0: 'value'}}";
        final var parser = Parser.objectOf(input);

        final var actualToken = parser.parse();

        final var actual = actualToken.reduce()
            .toObject().get("k0").reduce()
            .toObject().get("k0k0").reduce()
            .toLiteral();
        final var expected = "value";
        assertEquals(expected, actual);
    }

    @Test
    void getRemainder_SHOULD_returnEmptyString_WHEN_thereIsNoRemainder()
    {
        final var input = "{k: 'v'}";
        final var parser = Parser.objectOf(input);

        final var actual = parser.getRemainder();

        final var expected = "";
        assertEquals(expected, actual);
    }

    @Test
    void getRemainder_SHOULD_returnRemainder_WHEN_thereIsARemainder()
    {
        final var input = "{k: 'v'}remainder";
        final var parser = Parser.objectOf(input);

        final var actual = parser.getRemainder();

        final var expected = "remainder";
        assertEquals(expected, actual);
    }

    @Test
    void canParse_SHOULD_returnTrue_WHEN_inputIsAnEmptyObject()
    {
        final var input = "{}";
        final var parser = Parser.objectOf(input);

        final var actual = parser.canParse();

        final var expected = true;
        assertEquals(expected, actual);
    }

    @Test
    void canParse_SHOULD_returnTrue_WHEN_inputIsASingleFieldObject()
    {
        final var input = "{key: 'value'}";
        final var parser = Parser.objectOf(input);

        final var actual = parser.canParse();

        final var expected = true;
        assertEquals(expected, actual);
    }

    @Test
    void canParse_SHOULD_returnTrue_WHEN_inputHasARemainder()
    {
        final var input = "{k: ''}remainder";
        final var parser = Parser.objectOf(input);

        final var actual = parser.canParse();

        final var expected = true;
        assertEquals(expected, actual);
    }

    @Test
    void canParse_SHOULD_returnFalse_WHEN_inputIsALiteral()
    {
        final var input = "'this is a literal'";
        final var parser = Parser.objectOf(input);

        final var actual = parser.canParse();

        final var expected = false;
        assertEquals(expected, actual);
    }

    @Test
    void canParse_SHOULD_returnFalse_WHEN_inputIsAReference()
    {
        final var input = "reference";
        final var parser = Parser.objectOf(input);

        final var actual = parser.canParse();

        final var expected = false;
        assertEquals(expected, actual);
    }

    @Test
    void canParse_SHOULD_returnFalse_WHEN_inputIsMalformed()
    {
        final var input = "[k: '' [  ";
        final var parser = Parser.objectOf(input);

        final var actual = parser.canParse();

        final var expected = false;
        assertEquals(expected, actual);
    }
}
