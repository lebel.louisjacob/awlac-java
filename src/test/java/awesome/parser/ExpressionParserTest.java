package awesome.parser;

import awesome.expression.field.ReferenceNotFoundException;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

final class ExpressionParserTest
{
    @Test
    void parse_SHOULD_returnLiteralContent_WHEN_inputIsAValidLiteralExpression()
    {
        final var input = "'input'";
        final var parser = Parser.expressionOf(input);

        final var actualToken = parser.parse();

        final var actual = actualToken.reduce().toLiteral();
        final var expected = "input";
        assertEquals(expected, actual);
    }

    @Test
    void parse_SHOULD_returnConcatenatedLiteralContent_WHEN_inputIsAValidConcatenatedLiteralExpression()
    {
        final var input = "'in' :: 'pu' :: 't'";
        final var parser = Parser.expressionOf(input);

        final var actualToken = parser.parse();

        final var actual = actualToken.reduce().toLiteral();
        final var expected = "input";
        assertEquals(expected, actual);
    }

    @Test
    void parse_SHOULD_mergeObjectsTogether_WHEN_inputIsAValidConcatenatedObjectExpression()
    {
        final var input = "{k0: '0'}::{k1: '1'}.k1";
        final var parser = Parser.expressionOf(input);

        final var expression = parser.parse();

        final var actual = expression.reduce().toLiteral();
        final var expected = "1";
        assertEquals(expected, actual);
    }

    @Test
    void parse_SHOULD_throwMissingExpressionException_WHEN_inputConcatenatesExpressionWithNothing()
    {
        final var input = "'in' :: 'put' :: ";
        final var parser = Parser.expressionOf(input);

        assertThrows(MissingExpressionException.class, parser::parse);
    }

    @Test
    void parse_SHOULD_returnUpdatedObject_WHEN_inputIsObjectUpdate()
    {
        final var input = "{k0: '0'}::[k0: '1'].k0";
        final var parser = Parser.expressionOf(input);

        final var actualToken = parser.parse();

        final var actual = actualToken.reduce().toLiteral();
        final var expected = "1";
        assertEquals(expected, actual);
    }

    @Test
    void parse_SHOULD_returnUpdatedReference_WHEN_inputIsReferenceUpdate()
    {
        final var input = "{root: {k0: {field: '1'} k1: k0::[field: '2']}}.root.k1.field";
        final var parser = Parser.expressionOf(input);

        final var token = parser.parse();

        final var actual = token.reduce().toLiteral();
        final var expected = "2";
        assertEquals(expected, actual);
    }

    @Test
    void parse_SHOULD_returnLiteralContent_WHEN_inputIsLiteralAccess()
    {
        final var input = "{k0: '0'}.k0";
        final var parser = Parser.expressionOf(input);

        final var actualToken = parser.parse();

        final var actual = actualToken.reduce().toLiteral();
        final var expected = "0";
        assertEquals(expected, actual);
    }

    @Test
    void parse_SHOULD_returnObject_WHEN_inputIsObjectAccess()
    {
        final var input = "{k0: {k1: '1'}}.k0.k1";
        final var parser = Parser.expressionOf(input);

        final var token = parser.parse();

        final var actual = token.reduce().toLiteral();
        final var expected = "1";
        assertEquals(expected, actual);
    }

    @Test
    void parse_SHOULD_returnLiteral_WHEN_inputIsReferencedLiteral()
    {
        final var input = "{root: {k0: '0' k1: k0}}.root.k1";
        final var parser = Parser.expressionOf(input);

        final var token = parser.parse();

        final var actual = token.reduce().toLiteral();
        final var expected = "0";
        assertEquals(expected, actual);
    }

    @Test
    void parse_SHOULD_returnObject_WHEN_inputIsReferencedObject()
    {
        final var input = "{root: {k0: '0' k1: '1'}::[k1: k0]}.root.k1";
        final var parser = Parser.expressionOf(input);

        final var token = parser.parse();

        final var actual = token.reduce().toLiteral();
        final var expected = "0";
        assertEquals(expected, actual);
    }

    @Test
    void concat_SHOULD_permitRightToAccessLeft_WHEN_leftIsObject()
    {
        final var input = "{root: {k0: '0'}::{k1: k0}}.root.k1";
        final var parser = Parser.expressionOf(input);

        final var expression = parser.parse();

        final var actual = expression.reduce().toLiteral();
        final var expected = "0";
        assertEquals(expected, actual);
    }

    @Test
    void generate_SHOULD_throwMissingExpressionException_WHEN_inputConcatenatesNothingWithExpression()
    {
        final var input = ":: 'in' :: 'put'";
        final var parser = Parser.expressionOf(input);

        assertThrows(MissingExpressionException.class, parser::parse);
    }

    @Test
    void canParse_SHOULD_returnTrue_WHEN_inputIsAValidLiteralExpression()
    {
        final var input = "'input'";
        final var parser = Parser.expressionOf(input);

        final var actual = parser.canParse();

        final var expected = true;
        assertEquals(expected, actual);
    }

    @Test
    void canParse_SHOULD_returnTrue_WHEN_inputIsAValidConcatenatedLiteralExpression()
    {
        final var input = "'in' :: 'pu' :: 't'";
        final var parser = Parser.expressionOf(input);

        final var actual = parser.canParse();

        final var expected = true;
        assertEquals(expected, actual);
    }

    @Test
    void canParse_SHOULD_returnFalse_WHEN_theLastExpressionIsMissing()
    {
        final var input = "'in' :: 'put' :: ";
        final var parser = Parser.expressionOf(input);

        final var actual = parser.canParse();

        final var expected = false;
        assertEquals(expected, actual);
    }

    @Test
    void canParse_SHOULD_returnFalse_WHEN_theFirstExpressionIsMissing()
    {
        final var input = " :: 'in' :: 'put'";
        final var parser = Parser.expressionOf(input);

        final var actual = parser.canParse();

        final var expected = false;
        assertEquals(expected, actual);
    }

    @Test
    void getRemainder_SHOULD_throwMissingExpressionException_WHEN_anExpressionIsMissing()
    {
        final var input = "'in' :: 'put' :: ";
        final var parser = Parser.expressionOf(input);

        assertThrows(MissingExpressionException.class, parser::getRemainder);
    }
}
