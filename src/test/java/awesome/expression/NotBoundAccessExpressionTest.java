package awesome.expression;

import awesome.expression.field.Field;
import com.google.common.collect.ImmutableSet;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

final class NotBoundAccessExpressionTest
{
    @Test
    void compile_SHOULD_throwNotBoundAccessSyntaxException()
    {
        final var token = NotBoundAccessExpression.of("name");

        assertThrows(NotBoundAccessSyntaxException.class, token::reduce);
    }

    @Test
    void concat_SHOULD_returnTokenThatFailsToCompile()
    {
        final var access = NotBoundAccessExpression.of("k0");
        final var other = LiteralExpression.of("str");

        final var token = access.concatNotBound(other);

        assertThrows(NotBoundAccessSyntaxException.class, token::reduce);
    }

    @Test
    void visitForConcat_SHOULD_bindAccessToken_WHEN_previousIsObject()
    {
        final var access = NotBoundAccessExpression.of("k0");
        final var object = ObjectExpression.ownerOf(
            ImmutableSet.of(
                Field.of("k0", LiteralExpression.of("0"))));

        final var expression = access.visitForConcatNotBound(object);

        final var actual = expression.reduce().toLiteral();
        final var expected = "0";
        assertEquals(expected, actual);
    }

    @Test
    void visitForConcat_SHOULD_returnTokenThatFailsToCompile_WHEN_previousIsLiteral()
    {
        final var key = "k0";
        final var value = "0";
        final var access = NotBoundAccessExpression.of(key);
        final var literal = LiteralExpression.of(value);

        final var token = access.visitForConcatNotBound(literal);

        assertThrows(InvalidObjectCastException.class, token::reduce);
    }

    @Test
    void visitForConcat_SHOULD_failToCompile_WHEN_previousIsLiteral()
    {
        final var key = "k0";
        final var access = NotBoundAccessExpression.of(key);
        final var other = LiteralExpression.of("0");

        final var expression = access.visitForConcatNotBound(other);

        assertThrows(InvalidObjectCastException.class, expression::reduce);
    }
}
