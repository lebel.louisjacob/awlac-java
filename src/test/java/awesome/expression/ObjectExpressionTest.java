package awesome.expression;

import awesome.expression.field.DuplicatedReferenceException;
import awesome.expression.field.Field;
import com.google.common.collect.ImmutableSet;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

final class ObjectExpressionTest
{
    private static Expression expressionToReduce = null;

    @BeforeEach
    void setup()
    {
        expressionToReduce = ConcatExpression.of(
                LiteralExpression.of("0"),
                LiteralExpression.of("1"));
    }

    @Test
    void reduce_SHOULD_notChangeFields()
    {
        final var object = ObjectExpression.ownerOf(ImmutableSet.of(
            Field.of("k0", expressionToReduce)));

        final var terminal = object.reduce();

        assertEquals(object, terminal);
    }

    @Test
    void getAndReduce_SHOULD_reduceReturnedValue()
    {
        final var object = ObjectExpression.ownerOf(ImmutableSet.of(
            Field.of("k0", expressionToReduce)));

        final var terminal = object.get("k0");

        final var actual = terminal.reduce()
            .toLiteral();
        final var expected = "01";
        assertEquals(expected, actual);
    }

    @Test
    void findAndReduce_SHOULD_throw_WHEN_duplicatesAreFoundInObject()
    {
        final var nestedObject = ObjectExpression.ownerOf(ImmutableSet.of(
            Field.of("k0", expressionToReduce)));
        ObjectExpression.ownerOf(ImmutableSet.of(
            Field.of("k0", nestedObject)));

        assertThrows(DuplicatedReferenceException.class, () -> nestedObject.get("k0").find("k0"));
    }
}
