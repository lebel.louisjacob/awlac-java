package awesome.expression;

import awesome.expression.field.DuplicatedReferenceException;
import awesome.expression.field.Field;
import awesome.expression.field.ReferenceNotFoundException;
import com.google.common.collect.ImmutableSet;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;

import static org.junit.jupiter.api.Assertions.assertThrows;

final class ReferenceExpressionTest
{
    @Test
    void reduce_SHOULD_copyFarTerminal()
    {
        final var mock = Mockito.mock(Expression.class);
        final var expression = ObjectExpression.ownerOf(
            ImmutableSet.of(
                Field.of("k0", ReferenceExpression.of("k1")),
                Field.of("k1", mock)));
        final var reducedMock = Mockito.mock(TerminalExpression.class);
        Mockito.when(mock.reduce()).thenReturn(reducedMock);

        expression.reduce().toObject().get("k0")
            .reduce();

        Mockito.verify(reducedMock, Mockito.times(1)).copy();
    }

    @Test
    void reduce_SHOULD_throwWhenReferenceDoesNotExist()
    {
        final var expression = ReferenceExpression.of("nonexistent");
        Field.literalOf(expression);

        assertThrows(ReferenceNotFoundException.class, expression::reduce);
    }

    @Test
    void reduce_SHOULD_throwWhenReferenceIsDuplicated()
    {
        final var expression = ReferenceExpression.of("duplicated");
        ObjectExpression.ownerOf(
            ImmutableSet.of(
                Field.of("duplicated", LiteralExpression.of("0")),
                Field.literalOf(
                    ObjectExpression.ownerOf(
                        ImmutableSet.of(
                            Field.literalOf(expression),
                            Field.of("duplicated", LiteralExpression.of("1")))))));

        assertThrows(DuplicatedReferenceException.class, expression::reduce);
    }
}
