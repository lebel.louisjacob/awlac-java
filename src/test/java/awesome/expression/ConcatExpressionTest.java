package awesome.expression;

import awesome.expression.field.Field;
import awesome.expression.field.ReferenceNotFoundException;
import com.google.common.collect.ImmutableSet;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

final class ConcatExpressionTest
{
    @Test
    void reduce_SHOULD_returnObjectContainingLeft()
    {
        final var expression = ConcatExpression.of(
            ObjectExpression.ownerOf(
                ImmutableSet.of(
                    Field.of("left", LiteralExpression.of("0")))),
            ObjectExpression.of(
                ImmutableSet.of(
                    Field.of("right", LiteralExpression.of("1")))));

        final var reduced = expression.reduce();

        final var actual = reduced.toObject().get("left").reduce().toLiteral();
        final var expected = "0";
        assertEquals(expected, actual);
    }

    @Test
    void reduce_SHOULD_returnObjectContainingRight()
    {
        final var expression = ConcatExpression.of(
            ObjectExpression.ownerOf(
                ImmutableSet.of(
                    Field.of("left", LiteralExpression.of("0")))),
            ObjectExpression.of(
                ImmutableSet.of(
                    Field.of("right", LiteralExpression.of("1")))));

        final var reduced = expression.reduce();

        final var actual = reduced.toObject().get("right").reduce().toLiteral();
        final var expected = "1";
        assertEquals(expected, actual);
    }

    @Test
    void reduce_SHOULD_allowReferencesOfLeftInRight()
    {
        final var expression = ConcatExpression.of(
            ObjectExpression.ownerOf(
                ImmutableSet.of(
                    Field.of("left", LiteralExpression.of("0")))),
            ObjectExpression.of(
                ImmutableSet.of(
                    Field.of("right", ReferenceExpression.of("left")))));

        final var reduced = expression.reduce();

        final var actual = reduced.toObject().get("right").reduce().toLiteral();
        final var expected = "0";
        assertEquals(expected, actual);
    }

    @Test
    void reduce_SHOULD_disallowReferencesOfRightInLeft()
    {
        final var expression = ConcatExpression.of(
            ObjectExpression.ownerOf(
                ImmutableSet.of(
                    Field.of("left", ReferenceExpression.of("right")))),
            ObjectExpression.of(
                ImmutableSet.of(
                    Field.of("right", LiteralExpression.of("0")))));

        final var reduced = expression.reduce();

        final var actual = reduced.toObject().get("left");
        assertThrows(ReferenceNotFoundException.class, actual::reduce);
    }
}
